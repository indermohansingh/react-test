interface KidProps {
  name: { fname: string, lname: string }
}

export const Kid = ({ name }: KidProps) => {
  console.log('Kid rendered');
  return (
    <div>
      Child - {name.fname} {name.lname}
    </div>
  )
}