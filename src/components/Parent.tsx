import React, { useState } from 'react';
import { Kid } from './Kid';
import './Parent.css';

function Parent() {
  let [count] = useState(0);
  const name = {
    fname: 'Dark',
    lname: 'Knight'
  };

  const incrementCount = () => {
    count += 1;
    console.log(count);
  }

  const decrementCount = () => {
    count -= 1;
    console.log(count);
  }

  console.log('Parent rendered');
  return (
    <div className="container">
      <h1>Counter</h1>
      <button className="button" onClick={decrementCount}>-</button>
      <span className="text">{count}</span>
      <button className="button" onClick={incrementCount}>+</button>

      <h1>Kid</h1>
      <Kid name={name} />
    </div>
  );
}

export default Parent;
